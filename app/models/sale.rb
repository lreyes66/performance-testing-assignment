class Sale < ActiveRecord::Base
  belongs_to :customer
  include ActionView::Helpers::NumberHelper
  def amount_with_tax
    self.amount + self.tax
  end

end
